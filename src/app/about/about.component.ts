import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {
  typewriter_text: string = "About me"
  typewriter_display: string ="";
  wheatley: string="";

  constructor() { }

  ngOnInit(){
    this.typingCallBack(this);
    
  }

  typingCallBack(intro: this){
    let total_length = intro.typewriter_text.length;
    let current_length = intro.typewriter_display.length;
    if (current_length < total_length){
      intro.typewriter_display += intro.typewriter_text[current_length];
      setTimeout(intro.typingCallBack, 100, intro);
    }
    
    
  }
  

}
