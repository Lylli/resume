import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
  typewriter_text: string = "Natalie Heylen"
  typewriter_display: string ="";
  

  constructor() { }

  ngOnInit(){
    this.typingCallBack(this);
  }

  typingCallBack(intro: this){
    let total_length = intro.typewriter_text.length;
    let current_length = intro.typewriter_display.length;
    if (current_length < total_length){
      intro.typewriter_display += intro.typewriter_text[current_length];
      setTimeout(intro.typingCallBack, 200, intro);
    }

    
  }

  

}
