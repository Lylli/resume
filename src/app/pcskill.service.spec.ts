import { TestBed } from '@angular/core/testing';

import { PcskillService } from './pcskill.service';

describe('PcskillService', () => {
  let service: PcskillService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PcskillService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
