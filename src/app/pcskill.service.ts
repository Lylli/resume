import { Injectable } from '@angular/core';
import { pcSkills } from './resume/pcskills';

@Injectable({
  providedIn: 'root'
})
export class PcskillService {
  private pcSkill_List: pcSkills[] = [new pcSkills("/assets/java-59-1174952.png", "JAVA SE11"),
  new pcSkills("/assets/logo-html-5.png", "HTML 5"),
  new pcSkills("/assets/CSS3_logo_and_wordmark.svg.png", "CSS 3 en Bootstrap"),
  new pcSkills("/assets/Angular_full_color_logo.svg.png", "Angular"),
  new pcSkills("/assets/mysql.png", "MySQL"),
  new pcSkills("/assets/jdbc.png", "JDBC, JPA, Hibernate"),
  new pcSkills("/assets/junit.png", "JUnit, Maven, Mockito"),
  new pcSkills("/assets/spring-logo (1).png", "Spring Core, Spring Enterprise"),
  new pcSkills("/assets/docker-logo.png", "Docker, Jenkins")
]
  

constructor() { }

getPcSkill_List(): pcSkills[]{
  return [...this.pcSkill_List];
}

}
