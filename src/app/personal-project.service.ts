import { Injectable } from '@angular/core';
import { personalProject } from './portfolio/personalProject';

@Injectable({
  providedIn: 'root'
})
export class PersonalProjectService {
private personalProject_List: personalProject[] =[new personalProject("./assets/YouTube-Emblem.png", "Video-editing", "Lightworks Free Edition", "https://www.youtube.com/channel/UCXr17Mv8YRffQH-zmE60JPw", "I started my own Youtubechannel in August 2019 to explain how things worked in a MMORPG I was playing.  Right now this project is on hold since I stopped playing the game.  However I think this channel showcases my skills in video-editing and recording and manipulating voice-overs.  There is clear progress in quality comparing my first and last videos.", "Talking by myself into a microphone and listening to myself ramble about the game was defintely something new for me.  I also had challenges in figuring out how to video-edit, so I looked up tons of tutorials and kept trying until I was satisfied with the final product.  The most challenging part was finding content to talk about and to keep it interesting and short." )];
  constructor() { }
  
  getPersonalProject_List(): personalProject[]{
    return this.personalProject_List;
  }
  
  
}
