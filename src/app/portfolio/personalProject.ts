export class personalProject{   
   title: string;
   description: string;
   image: string;
   code: string;
   titelcontent: string;
   content: string;


   constructor(image: string, title: string, description: string,  code: string,  titelcontent: string, content: string){
      this.image= image;
      this.title=title;
      this.description =description;
      this.code= code;
      this.content= content;
      this.titelcontent= titelcontent;
   }
}