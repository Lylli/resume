import { Component, OnInit } from '@angular/core';
import { PersonalProjectService } from '../personal-project.service';
import { SchoolProjectService } from '../school-project.service';
import { personalProject } from './personalProject';
import { schoolProject } from './schoolProject';

@Component({
  selector: 'app-portfolio',
  templateUrl: './portfolio.component.html',
  styleUrls: ['./portfolio.component.css']
})
export class PortfolioComponent implements OnInit {
  popup: boolean[] = [];
  popup2: boolean[] = [];
  popup3: boolean[] = [];
  popup4: boolean[] = [];
  personalProject_List: personalProject[];

  schoolProject_List: schoolProject[];

  
  
  constructor(private service: PersonalProjectService, private service2: SchoolProjectService ) {
    this.personalProject_List = service.getPersonalProject_List();
    this.schoolProject_List = service2.getSchoolProject_List(); 
   }

  ngOnInit(): void {
    for (let i = 0; i < this.schoolProject_List.length; i++){
      this.popup[i] = false;
      this.popup2[i] = false;
      
    }
    for (let i = 0; i < this.personalProject_List.length; i++){
      this.popup3[i] = false;
      this.popup4[i] = false;
    }
  }

  getPersonalProject_List(){
    return this.service.getPersonalProject_List();
  }

  getSchoolProject_List(){
    return this.service2.getSchoolProject_List();
  }
}
