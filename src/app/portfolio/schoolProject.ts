import { Input } from "@angular/core";
import { Button } from "protractor";
import { InputType } from "zlib";

export class schoolProject{   
   title: string;
   description: string;
   image: string;
   code: string;
   
   titelcontent: string;
   content: string;


   constructor(image: string, title: string, description: string,  code: string,  titelcontent: string, content: string){
      this.image= image;
      this.title=title;
      this.description =description;
      this.code= code;
      this.content= content;
      this.titelcontent= titelcontent;
   }
   
}