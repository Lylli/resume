export class pcSkills{   
   namePcSkill: string;
   
   image: string;

   constructor(image: string, namePcSkill: string){
      this.namePcSkill= namePcSkill;
      
      this.image =image;
   }
}