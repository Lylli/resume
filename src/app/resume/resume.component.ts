import { Component, OnInit } from '@angular/core';
import { PcskillService } from '../pcskill.service';
import { SkillService } from '../skill.service';
import { pcSkills } from './pcskills';
import { Skills } from './skills';

@Component({
  selector: 'app-resume',
  templateUrl: './resume.component.html',
  styleUrls: ['./resume.component.css']
})
export class ResumeComponent implements OnInit {
  typewriter_text: string = "Resume";
  typewriter_display: string ="";
  isShownEdu: boolean = false;
  skill_List: Skills[];
  pcSkill_List: pcSkills[];
  isShownSkill: boolean = false;
  isShownExp: boolean = false;
  

  constructor(private service: SkillService, private service2: PcskillService) {
    this.skill_List=service.getSkill_List();
    this.pcSkill_List =service2.getPcSkill_List();
   }

  ngOnInit(){
    this.typingCallBack(this);
  }

  typingCallBack(intro: this){
    let total_length = intro.typewriter_text.length;
    let current_length = intro.typewriter_display.length;
    if (current_length < total_length){
      intro.typewriter_display += intro.typewriter_text[current_length];
      setTimeout(intro.typingCallBack, 100, intro);
    }
  }

  toggleShowEdu(){
    this.isShownEdu = !this.isShownEdu;
    }
  toggleShowSkill(){
    this.isShownSkill = !this.isShownSkill;
      }
  toggleShowExp(){
        this.isShownExp = !this.isShownExp;
          }
    public getSkill_List(){
      return this.service.getSkill_List();
    }
  
    public getPcSkill_List(){
      return this.service2.getPcSkill_List();
    }
    


}
