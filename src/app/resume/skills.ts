// Languages
export class Skills{   
   nameSkill: string;
   descriptionSkill: string;

   constructor(nameSkill: string, descriptionSkill: string){
      this.nameSkill= nameSkill;
      this.descriptionSkill=descriptionSkill;
   }
}