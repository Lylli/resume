import { TestBed } from '@angular/core/testing';

import { SchoolProjectService } from './school-project.service';

describe('SchoolProjectService', () => {
  let service: SchoolProjectService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SchoolProjectService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
