import { Injectable } from '@angular/core';
import { schoolProject } from './portfolio/schoolProject';

@Injectable({
  providedIn: 'root'
})
export class SchoolProjectService {
  private schoolProject_List: schoolProject[] = [new schoolProject("./assets/java-59-1174952.png", "Java Beginners", "First project: Restaurant-app", "https://gitlab.com/Lylli/restaurantapp", "In this project I made a restaurant-application.  The goal was to see a list of restaurants and be able to add restuarants (as an owner), add dishes, adjust names and prices etc.  The extra challenge was to log in as a customer and to be able to order at a certain restaurant and select certain dishes", "This was our first project working in Java.  It was difficult to figure everything out because the beginning was overwhelming with all the objects that needed to be in the application.  I used arrays in this project because at that time we didn't learn to use lists yet.  I prefer to leave the code as it is, because it was our first project and reminds me how far I've come." ),

  new schoolProject("./assets/java-59-1174952.png", "Java Gevorderd", "Second project: Legend of the Lamb", "https://gitlab.com/Lylli/legendofthelamb", "For our second project, we made something I really loved: a textbased RPG game.  We got some instructions on how the game should work and look, but certain parts (like sentences npc's might say) were completely up to us.  The goal of the game was to ensure to have playercreation, battles and a map to walk on.  Next to that you would have to be able to start a new game, save and load the game as well.", 'Although the project was really fun to make, there were a couple of challenges that came with it. We started with the playercreation since there were a lot of abilities we needed to keep track of. Sometimes it was so much, we got lost in our own code. By making the names of our classes easy understandable, it became a bit easier.  Also making monsters interact with the playerclass was fun to do. We had to use debug a lot to figure out our mistakes. Overal the project was challenging and fun.'),
  
  new schoolProject("./assets/csshtmlang.png", "HTML CSS Angular", "Third project: Website", "https://www.multimedi-education.be/Wondelgem", "This project consisted out of making a resume website and the jobdaywebsite. The resumewebsite is the website you're currently on.  I made the jobdaywebsite together with a colleague and ours was chosen to be put online. You can check this website out on the link provided below.", 'I made this website in Angular, which I know is a bit overkill.  We were free to only use HTML, CSS and JavaScript if we wanted, but I wanted to practise in Angular, so that is why I made this decision.  I had to look up a lot of stuff online, and I believe it is still not finished.  I would have loved to put the Wheatly icon in an animation going across the screen with maybe a textbubble, but I could not figure out how yet.'),

  new schoolProject("./assets/shutterstock_395761765-scaled.jpg", "Maven, JDBC, Hibernate, mySQL, JUnit", "Fourth project: Takeout App", "https://gitlab.com/Lylli/project-4-masay", "For this project we worked alone and were asked to make an app that is linked to a database so users can log in and order food.", "Again we got a lot of features to make and little time to do it.  I decided to prioritise my connection to the database and a functional app above the testing.  I tried testing on the last day when my app was somewhat functional but I did struggle a bit with the testdatabase. This is definetly an area I need to improve on.")]

constructor() { }

getSchoolProject_List(): schoolProject[]{
  return this.schoolProject_List;
}
}
