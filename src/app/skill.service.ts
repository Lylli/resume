import { Injectable } from '@angular/core';
import { Skills } from './resume/skills';

// Skillservice is voor de array van talen
@Injectable({
  providedIn: 'root'
})
export class SkillService {
  private skill_List: Skills[] = [new Skills ("Dutch: ","excellent"),
                                new Skills ("English: ","excellent"),
                                new Skills ("French: ","average"),
                                new Skills ("German: ","basic"),
                                new Skills ("Japanese: ","basic")];
  constructor() { }

  getSkill_List(): Array<Skills>{
    return [...this.skill_List];
  }

  
}
